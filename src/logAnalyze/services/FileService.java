package logAnalyze.services;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by siliku on 4/12/2017 AD.
 */
public class FileService {

    public static List<String> readFile(File fin) throws IOException {

        List<String> result = new ArrayList<>();

        // Construct BufferedReader from FileReader
        BufferedReader br = new BufferedReader(new FileReader(fin));
        String line = null;
        while ((line = br.readLine()) != null) {
            result.add(line);
        }
        br.close();

        return  result;
    }

    public static List<String> listAllFile(String folderPath){
        File folder = new File(folderPath);
        File[] listOfFiles = folder.listFiles();
        List<String> result = new ArrayList<>();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                result.add(listOfFiles[i].getName());
            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
            }
        }
        return result;
    }
}
