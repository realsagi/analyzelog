package logAnalyze;

import logAnalyze.services.FileService;

import java.io.File;
import java.util.List;
import java.util.regex.Pattern;

public class Main {

    private static String[] pathCusotmerPortal = {
            "/Users/siliku/Documents/bluebik/project-my-channel/program_analyze_log/Log-MyChannel/web151/app/mychannel/logs/service/customerportal",
            "/Users/siliku/Documents/bluebik/project-my-channel/program_analyze_log/Log-MyChannel/web152/app/mychannel/logs/service/customerportal"
    };

    private static String[] pathAuthentication = {
            "/Users/siliku/Documents/bluebik/project-my-channel/program_analyze_log/Log-MyChannel/app118/app/service-authentication/logs/info",
            "/Users/siliku/Documents/bluebik/project-my-channel/program_analyze_log/Log-MyChannel/app119/app/service-authentication/logs/info"
    };

    private static String[] pathSSBItProfile = {
            "/Users/siliku/Documents/bluebik/project-my-channel/program_analyze_log/Log-MyChannel/app118/app2/service-customer-portal/logs/service/ssb-it-profile",
            "/Users/siliku/Documents/bluebik/project-my-channel/program_analyze_log/Log-MyChannel/app119/app2/service-customer-portal/logs/service/ssb-it-profile"
    };
    private static String[] pathSSBItTransfrom = {
            "/Users/siliku/Documents/bluebik/project-my-channel/program_analyze_log/Log-MyChannel/app118/app2/service-customer-portal/logs/service/ssb-it-transform",
            "/Users/siliku/Documents/bluebik/project-my-channel/program_analyze_log/Log-MyChannel/app119/app2/service-customer-portal/logs/service/ssb-it-transform"
    };
    private static String[] pathSSBItOmniChannel = {
            "/Users/siliku/Documents/bluebik/project-my-channel/program_analyze_log/Log-MyChannel/app118/app2/service-customer-portal/logs/service/ssb-omni-channel",
            "/Users/siliku/Documents/bluebik/project-my-channel/program_analyze_log/Log-MyChannel/app119/app2/service-customer-portal/logs/service/ssb-omni-channel"
    };

    public static void main(String[] args) {
        analyzeCusotmerportal();
        analyzeAuthentication();
        analyzeSsbItProfile();
        analyzeSsbItTransform();
        analyzeSsbOmni();
    }

    private static void analyzeCusotmerportal() {
        int countLog[] = countLog(pathCusotmerPortal);
        int zeroToOne = countLog[0];
        int oneToTwo = countLog[1];
        int twoToFive = countLog[2];
        int fiveToTen = countLog[3];
        int moreThanTen = countLog[4];
        System.out.println("========================================");
        System.out.println("Customer Portal");
        System.out.println("0-1: " + zeroToOne);
        System.out.println("1-2: " + oneToTwo);
        System.out.println("2-5: " + twoToFive);
        System.out.println("5-10: " + fiveToTen);
        System.out.println("10 >: " + moreThanTen);
        System.out.println("========================================");
    }

    private static void analyzeAuthentication() {
        int countLog[] = countLog(pathAuthentication);
        int zeroToOne = countLog[0];
        int oneToTwo = countLog[1];
        int twoToFive = countLog[2];
        int fiveToTen = countLog[3];
        int moreThanTen = countLog[4];
        System.out.println("========================================");
        System.out.println("Service Authentication");
        System.out.println("0-1: " + zeroToOne);
        System.out.println("1-2: " + oneToTwo);
        System.out.println("2-5: " + twoToFive);
        System.out.println("5-10: " + fiveToTen);
        System.out.println("10 >: " + moreThanTen);
        System.out.println("========================================");
    }

    private static void analyzeSsbItProfile() {
        int countLog[] = countLog(pathSSBItProfile);
        int zeroToOne = countLog[0];
        int oneToTwo = countLog[1];
        int twoToFive = countLog[2];
        int fiveToTen = countLog[3];
        int moreThanTen = countLog[4];
        System.out.println("========================================");
        System.out.println("SSB IT PROFILE");
        System.out.println("0-1: " + zeroToOne);
        System.out.println("1-2: " + oneToTwo);
        System.out.println("2-5: " + twoToFive);
        System.out.println("5-10: " + fiveToTen);
        System.out.println("10 >: " + moreThanTen);
        System.out.println("========================================");

    }

    private static void analyzeSsbItTransform() {
        int countLog[] = countLog(pathSSBItTransfrom);
        int zeroToOne = countLog[0];
        int oneToTwo = countLog[1];
        int twoToFive = countLog[2];
        int fiveToTen = countLog[3];
        int moreThanTen = countLog[4];
        System.out.println("========================================");
        System.out.println("SSB IT TRANSFORM");
        System.out.println("0-1: " + zeroToOne);
        System.out.println("1-2: " + oneToTwo);
        System.out.println("2-5: " + twoToFive);
        System.out.println("5-10: " + fiveToTen);
        System.out.println("10 >: " + moreThanTen);
        System.out.println("========================================");
    }


    private static void analyzeSsbOmni() {
        int countLog[] = countLog(pathSSBItOmniChannel);
        int zeroToOne = countLog[0];
        int oneToTwo = countLog[1];
        int twoToFive = countLog[2];
        int fiveToTen = countLog[3];
        int moreThanTen = countLog[4];
        System.out.println("========================================");
        System.out.println("SSB IT OMNI CHANNEL");
        System.out.println("0-1: " + zeroToOne);
        System.out.println("1-2: " + oneToTwo);
        System.out.println("2-5: " + twoToFive);
        System.out.println("5-10: " + fiveToTen);
        System.out.println("10 >: " + moreThanTen);
        System.out.println("========================================");
    }

    private static int[] countLog(String[] pathFile) {
        int count[] = {0, 0, 0, 0, 0};
        try {
            for (String path : pathFile) {
                List<String> fileNames = FileService.listAllFile(path);
                for (String fileName : fileNames) {
                    File file = new File(path + "/" + fileName);
                    List<String> dataList = FileService.readFile(file);
                    for (String data : dataList) {
                        String[] dataSplit = data.split(Pattern.quote("|"));
                        if (isNumeric(dataSplit[11])) {
                            float timeResponse = Float.parseFloat(dataSplit[11]);
                            if (0 <= timeResponse && timeResponse < 1000) {
                                count[0]++;
                            } else if (1000 <= timeResponse && timeResponse < 2000) {
                                count[1]++;
                            } else if (2000 <= timeResponse && timeResponse < 5000) {
                                count[2]++;
                            } else if (5000 <= timeResponse && timeResponse < 10000) {
                                count[3]++;
                            } else if (10000 <= timeResponse) {
                                count[4]++;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return count;
    }

    private static boolean isNumeric(String s) {
        return s.matches("[-+]?\\d*\\.?\\d+");
    }

}
